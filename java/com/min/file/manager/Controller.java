package com.min.file.manager;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.VBox;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Controller {
    @FXML
    VBox leftPanel, rightPanel;

    public void btnExitAction(ActionEvent actionEvent) {
        Platform.exit();
    }

    public void deleteBtnAction(ActionEvent actionEvent) {
        PanelControl leftPC = (PanelControl) leftPanel.getProperties().get("ctrl");
        PanelControl rightPC = (PanelControl) rightPanel.getProperties().get("ctrl");

        if (leftPC.getSelectedFilename() == null && rightPC.getSelectedFilename() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Ни один файл не был выбран", ButtonType.OK);
            alert.showAndWait();
            return;
        }
        if (leftPC.getSelectedFilename() != null) {
            File file = new File(leftPC.getSelectedFilename());
            if (file.delete()) {
                Alert alertDelete = new Alert(Alert.AlertType.ERROR, "Файл удален", ButtonType.OK);
                alertDelete.showAndWait();
                leftPC.updateList(Paths.get(leftPC.getCurrentPath()));
            }


        }
        if (rightPC.getSelectedFilename() != null) {
            File file = new File(rightPC.getSelectedFilename());
            if (file.delete()) {
                Alert alertDelete = new Alert(Alert.AlertType.ERROR, "Файл удален", ButtonType.OK);
                alertDelete.showAndWait();
                rightPC.updateList(Paths.get(rightPC.getCurrentPath()));
            }
        }
    }

    public void copyBtnAction(ActionEvent actionEvent) {
        PanelControl leftPC = (PanelControl) leftPanel.getProperties().get("ctrl");
        PanelControl rightPC = (PanelControl) rightPanel.getProperties().get("ctrl");

        if (leftPC.getSelectedFilename() == null && rightPC.getSelectedFilename() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Ни один файл не был выбран", ButtonType.OK);
            alert.showAndWait();
            return;
        }

        PanelControl srcPC = null, dstPC = null;
        if (leftPC.getSelectedFilename() != null) {
            srcPC = leftPC;
            dstPC = rightPC;
        }
        if (rightPC.getSelectedFilename() != null) {
            srcPC = rightPC;
            dstPC = leftPC;
        }

        Path srcPath = Paths.get(srcPC.getCurrentPath(), srcPC.getSelectedFilename());
        Path dstPath = Paths.get(dstPC.getCurrentPath()).resolve(srcPath.getFileName().toString());

        try {
            Files.copy(srcPath, dstPath);
            dstPC.updateList(Paths.get(dstPC.getCurrentPath()));
        } catch (IOException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Не удалось скопировать указанный файл", ButtonType.OK);
            alert.showAndWait();
        }
    }

    public void moveBtnAction(ActionEvent actionEvent) {
        PanelControl leftPC = (PanelControl) leftPanel.getProperties().get("ctrl");
        PanelControl rightPC = (PanelControl) rightPanel.getProperties().get("ctrl");

        if (leftPC.getSelectedFilename() == null && rightPC.getSelectedFilename() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Ни один файл не был выбран", ButtonType.OK);
            alert.showAndWait();
            return;
        }
        PanelControl srcPC = null, dstPC = null;
        if (leftPC.getSelectedFilename() != null) {
            srcPC = leftPC;
            dstPC = rightPC;
        }
        if (rightPC.getSelectedFilename() != null) {
            srcPC = rightPC;
            dstPC = leftPC;
        }
        Path srcPath = Paths.get(srcPC.getCurrentPath(), srcPC.getSelectedFilename());
        Path dstPath = Paths.get(dstPC.getCurrentPath()).resolve(srcPath.getFileName().toString());

        File file = (new File(srcPath.toString()));
        file.renameTo(new File(dstPath.toString()));
        file.delete();
        Alert alert = new Alert(Alert.AlertType.ERROR, "Файл перемещен", ButtonType.OK);
        alert.showAndWait();
        srcPC.updateList(Paths.get(srcPC.getCurrentPath()));
        dstPC.updateList(Paths.get(dstPC.getCurrentPath()));

    }
}